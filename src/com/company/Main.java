package com.company;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(args[0]);

        byte[] array = new byte[fileInputStream.available()];
        while (fileInputStream.available() > 0) {
            fileInputStream.read(array);
        }
        fileInputStream.close();

        Map<Byte, Integer> map = new TreeMap<>();
        for (byte b : array) {
            map.put(b, map.getOrDefault(b, 0) + 1);
        }
        for (Map.Entry<Byte, Integer> entry : map.entrySet()) {
            System.out.println((char) (byte) entry.getKey() + " " + entry.getValue());
        }
    }
}
